

/*
--------------------------------------------------
- Mobile Menu
--------------------------------------------------
*/
$(document).on('click', '.nav_menu_btn, .nav_menu_bg', function () {
	$('.nav_menu_btn').toggleClass('active');
	$('.nav_menu').toggleClass('active');
	$('.nav_menu_bg').toggleClass('active');
	$('html, body').toggleClass('ov-h');
});
/*--------------- Mobile Menu ---------------*/






/*
--------------------------------------------------
- Header Banner
--------------------------------------------------
*/
$(document).ready(function () {
	try {
		var owl = $('.hdr_bnr');

		owl.owlCarousel({
			// autoPlay: 5000,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			addClassActive: true,
			slideSpeed: 300,
			paginationSpeed: 400,
			transitionStyle : 'custom'
		});

		// Custom Navigation Events
		$('.hdr_bnr_btn.prev').click(function () {
			owl.trigger('owl.prev');
		});

		$('.hdr_bnr_btn.next').click(function () {
			owl.trigger('owl.next');
		});

	}
	catch (e) {
		console.warn("Owl Carousel cannot find .hdr_bnr");
	}
});
/*--------------- Header Banner ---------------*/






/*
--------------------------------------------------
- Comment Banner
--------------------------------------------------
*/
$(window).on('load', function () {
	try {
		var owl = $('.cmt_bnr');

		owl.owlCarousel({
			autoPlay: 4000,
			stopOnHover: true,
			itemsCustom : [
				[0, 1],
				[480, 2],
				[992, 3],
				[1200, 4],
			],
			// autoHeight : true,
			navigation: false,
			slideSpeed: 300,
			paginationSpeed: 400,
		});

	}
	catch (e) {
		console.warn("Owl Carousel cannot find .cmt_bnr");
	}
});
/*--------------- Comment Banner ---------------*/






/*
--------------------------------------------------
- Partners Banner
--------------------------------------------------
*/
$(document).ready(function () {
	try {
		var owl = $('.part_bnr1');

		owl.owlCarousel({
			// autoPlay: 4000,
			stopOnHover: true,
			itemsCustom : [
				[0, 2],
				[480, 3],
				[992, 4],
				[1200, 5],
			],
			// autoHeight : true,
			navigation: false,
			slideSpeed: 300,
			paginationSpeed: 400,
		});


		// Custom Navigation Events
		$('.part_bnr_btn1.prev').click(function () {
			owl.trigger('owl.prev');
		});

		$('.part_bnr_btn1.next').click(function () {
			owl.trigger('owl.next');
		});

	}
	catch (e) {
		console.warn("Owl Carousel cannot find .part_bnr");
	}
});


$(document).ready(function () {
	try {
		var owl = $('.part_bnr2');

		owl.owlCarousel({
			// autoPlay: 4000,
			stopOnHover: true,
			itemsCustom : [
				[0, 2],
				[480, 3],
				[992, 4],
				[1200, 5],
			],
			// autoHeight : true,
			navigation: false,
			slideSpeed: 300,
			paginationSpeed: 400,
		});


		// Custom Navigation Events
		$('.part_bnr_btn2.prev').click(function () {
			owl.trigger('owl.prev');
		});

		$('.part_bnr_btn2.next').click(function () {
			owl.trigger('owl.next');
		});

	}
	catch (e) {
		console.warn("Owl Carousel cannot find .part_bnr");
	}
});
/*--------------- Partners Banner ---------------*/










/*
--------------------------------------------------
- Exhibition Summary
--------------------------------------------------
*/
$(document).on('click', '.exib_sum_it_ttl', function () {

	$(this).parent().toggleClass('active');

	$(this).siblings('.exib_sum_it_cnt').slideToggle();

});
/*--------------- Exhibition Summary ---------------*/







/*
--------------------------------------------------
- News Banner
--------------------------------------------------
*/
$(document).ready(function () {
	try {
		var owl = $('.exib_news_one_bnr');

		owl.owlCarousel({
			autoPlay: 4500,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			slideSpeed: 300,
			paginationSpeed: 400,
		});

		// Custom Navigation Events
		$('.exib_news_one_bnr_btn.prev').click(function () {
			owl.trigger('owl.prev');
		});

		$('.exib_news_one_bnr_btn.next').click(function () {
			owl.trigger('owl.next');
		});

	}
	catch (e) {
		console.warn("Owl Carousel cannot find .exib_news_one_bnr");
	}
});
/*--------------- News Banner ---------------*/







/*
--------------------------------------------------
- Participant Reserve
--------------------------------------------------
*/
$(document).ready(function () {
	try {
		var owl = $('.ptcp_rsv_info_bnr');

		owl.owlCarousel({
			// autoPlay: 4500,
			stopOnHover: true,
			singleItem: true,
			autoHeight : true,
			navigation: false,
			slideSpeed: 300,
			paginationSpeed: 400,
			lazyLoad : true,
		});

		// Custom Navigation Events
		$('.ptcp_rsv_info_bnr_btn.prev').click(function () {
			owl.trigger('owl.prev');
		});

		$('.ptcp_rsv_info_bnr_btn.next').click(function () {
			owl.trigger('owl.next');
		});

	}
	catch (e) {
		console.warn("Owl Carousel cannot find .ptcp_rsv_info_bnr");
	}
});
/*--------------- Participant Reserve ---------------*/








/*
--------------------------------------------------
- Checkbox
--------------------------------------------------
*/
$(document).on('click', '.vis_go_chk', function () {
	$(this).toggleClass('active');

	var inputCheck = $(this).siblings('.vis_go_chk_inp');
	inputCheck.prop('checked', !inputCheck.prop('checked'));
});
/*--------------- Checkbox ---------------*/








/*
--------------------------------------------------
- Participant Switch
--------------------------------------------------
*/
$(document).on('click', '.vis_ptc_all_it_ctrl a', function (e) {
	e.preventDefault();


	$(this).toggleClass('active');

	$(this).closest('.vis_ptc_all_it').find('.vis_ptc_all_it_bd').slideToggle();
});
/*--------------- Participant Switch ---------------*/


/*
--------------------------------------------------
- Participant Open All
--------------------------------------------------
*/
$(document).on('click', '.vis_ptc_all_list_ctrl a', function (e) {
	e.preventDefault();

	if ($(this).hasClass('active'))
	{
		$(this).removeClass('active');
		$('.vis_ptc_all_it_bd').slideUp();
	}
	else
	{
		$(this).addClass('active');
		$('.vis_ptc_all_it_bd').slideDown();
	}
});
/*--------------- Participant Open all ---------------*/


















/*
--------------------------------------------------
- Header
--------------------------------------------------
*/

/*--------------- Header ---------------*/


